module example.com/hamravesh-task

go 1.16

require (
	github.com/aws/constructs-go/constructs/v10 v10.1.93
	github.com/aws/constructs-go/constructs/v3 v3.4.86
	github.com/aws/jsii-runtime-go v1.66.0
	github.com/cdk8s-team/cdk8s-core-go/cdk8s v1.7.18
	github.com/cdk8s-team/cdk8s-core-go/cdk8s/v2 v2.4.18
)
