package main

import (
	"github.com/aws/constructs-go/constructs/v10"
	"github.com/aws/jsii-runtime-go"
	"github.com/cdk8s-team/cdk8s-core-go/cdk8s/v2"

	"example.com/hamravesh-task/imports/k8s"
)

type MyChartProps struct {
	cdk8s.ChartProps
}

const (
	mysqlDBNameKey       = "MYSQL_DATABASE"
	mysqlRootPasswordKey = "MYSQL_ROOT_PASSWORD"
	mysqlUserKey         = "MYSQL_USER"
	mysqlPasswordKey     = "MYSQL_PASSWORD"

	nextcloudHost = "hamravesh.ahmadkarimi12.ir"
)

func NewMyChart(scope constructs.Construct, id string, props *MyChartProps) cdk8s.Chart {
	var cprops cdk8s.ChartProps
	if props != nil {
		cprops = props.ChartProps
	}
	chart := cdk8s.NewChart(scope, jsii.String(id), &cprops)

	mariadbLabels := map[string]*string{"app": jsii.String("mariadb")}
	nextcloudLabels := map[string]*string{"app": jsii.String("nextcloud")}

	k8s.NewKubeService(chart, jsii.String("mariadb-service"), &k8s.KubeServiceProps{
		Metadata: &k8s.ObjectMeta{
			Name:   jsii.String("mariadb"),
			Labels: &mariadbLabels,
		},
		Spec: &k8s.ServiceSpec{
			Type: jsii.String("ClusterIP"),
			Ports: &[]*k8s.ServicePort{{
				Port: jsii.Number(3306),
				Name: jsii.String("mariadb"),
			}},
			Selector: &mariadbLabels,
		},
	})

	k8s.NewKubeSecret(chart, jsii.String("mariadb-secret"), &k8s.KubeSecretProps{
		Metadata: &k8s.ObjectMeta{
			Name: jsii.String("mariadb"),
		},
		StringData: &map[string]*string{
			mysqlUserKey:         jsii.String("nextcloud"),
			mysqlDBNameKey:       jsii.String("nextcloud"),
			mysqlRootPasswordKey: jsii.String("aSecurePassword1@"),
			mysqlPasswordKey:     jsii.String("!2anotherpasSword"),
		},
	})

	k8s.NewKubeStatefulSet(chart, jsii.String("mariadb-statefulset"), &k8s.KubeStatefulSetProps{
		Metadata: &k8s.ObjectMeta{
			Name:   jsii.String("mariadb"),
			Labels: &mariadbLabels,
		},
		Spec: &k8s.StatefulSetSpec{
			ServiceName: jsii.String("mariadb"),
			Replicas:    jsii.Number(1),
			Selector: &k8s.LabelSelector{
				MatchLabels: &mariadbLabels,
			},
			Template: &k8s.PodTemplateSpec{
				Metadata: &k8s.ObjectMeta{
					Labels: &mariadbLabels,
				},
				Spec: &k8s.PodSpec{
					Containers: &[]*k8s.Container{
						&k8s.Container{
							Name:  jsii.String("mariadb"),
							Image: jsii.String("mariadb:10.9"),
							Ports: &[]*k8s.ContainerPort{
								&k8s.ContainerPort{
									ContainerPort: jsii.Number(3306),
									Name:          jsii.String("mariadb"),
								},
							},
							EnvFrom: &[]*k8s.EnvFromSource{
								&k8s.EnvFromSource{SecretRef: &k8s.SecretEnvSource{
									Name: jsii.String("mariadb"),
								}},
							},
							VolumeMounts: &[]*k8s.VolumeMount{
								&k8s.VolumeMount{
									Name:      jsii.String("datadir"),
									MountPath: jsii.String("/var/lib/mysql/"),
								},
							},
						},
					},
				},
			},
			VolumeClaimTemplates: &[]*k8s.KubePersistentVolumeClaimProps{
				&k8s.KubePersistentVolumeClaimProps{
					Metadata: &k8s.ObjectMeta{
						Name: jsii.String("datadir"),
					},
					Spec: &k8s.PersistentVolumeClaimSpec{
						AccessModes:      jsii.Strings("ReadWriteOnce"),
						StorageClassName: jsii.String("rawfile-btrfs"),
						Resources: &k8s.ResourceRequirements{
							Requests: &map[string]k8s.Quantity{
								"storage": k8s.Quantity_FromString(jsii.String("500M")),
							},
						},
					},
				},
			},
		},
	})

	k8s.NewKubeSecret(chart, jsii.String("nextcloud-secret"), &k8s.KubeSecretProps{
		Metadata: &k8s.ObjectMeta{
			Name: jsii.String("nextcloud"),
		},
		StringData: &map[string]*string{
			"NEXTCLOUD_ADMIN_USER":      jsii.String("nextcloud"),
			"NEXTCLOUD_ADMIN_PASSWORD":  jsii.String("nextcloudadmin"),
			"NEXTCLOUD_TRUSTED_DOMAINS": jsii.String("hamravesh.ahmadkarimi12.ir"),
		},
	})

	k8s.NewKubeService(chart, jsii.String("nextcloud-service"), &k8s.KubeServiceProps{
		Metadata: &k8s.ObjectMeta{
			Name:   jsii.String("nextcloud"),
			Labels: &nextcloudLabels,
		},
		Spec: &k8s.ServiceSpec{
			Type: jsii.String("ClusterIP"),
			Ports: &[]*k8s.ServicePort{{
				Port: jsii.Number(80),
				Name: jsii.String("nextcloud"),
			}},
			Selector: &nextcloudLabels,
		},
	})

	k8s.NewKubeStatefulSet(chart, jsii.String("nextcloud-statefulset"), &k8s.KubeStatefulSetProps{
		Metadata: &k8s.ObjectMeta{
			Name: jsii.String("nextcloud"),
		},
		Spec: &k8s.StatefulSetSpec{
			ServiceName: jsii.String("nextcloud"),
			Selector: &k8s.LabelSelector{
				MatchLabels: &nextcloudLabels,
			},
			Replicas: jsii.Number(1),
			Template: &k8s.PodTemplateSpec{
				Metadata: &k8s.ObjectMeta{
					Labels: &nextcloudLabels,
				},
				Spec: &k8s.PodSpec{
					Containers: &[]*k8s.Container{
						&k8s.Container{
							Name:  jsii.String("nextcloud"),
							Image: jsii.String("nextcloud:apache"),
							Ports: &[]*k8s.ContainerPort{
								&k8s.ContainerPort{
									ContainerPort: jsii.Number(80),
								},
							},
							Env: &[]*k8s.EnvVar{
								&k8s.EnvVar{
									Name:  jsii.String("MYSQL_HOST"),
									Value: jsii.String("mariadb"),
								},
								&k8s.EnvVar{
									Name: jsii.String(mysqlDBNameKey),
									ValueFrom: &k8s.EnvVarSource{
										SecretKeyRef: &k8s.SecretKeySelector{
											Name: jsii.String("mariadb"),
											Key:  jsii.String(mysqlDBNameKey),
										},
									},
								},
								&k8s.EnvVar{
									Name: jsii.String(mysqlUserKey),
									ValueFrom: &k8s.EnvVarSource{
										SecretKeyRef: &k8s.SecretKeySelector{
											Name: jsii.String("mariadb"),
											Key:  jsii.String(mysqlUserKey),
										},
									},
								},
								&k8s.EnvVar{
									Name: jsii.String(mysqlPasswordKey),
									ValueFrom: &k8s.EnvVarSource{
										SecretKeyRef: &k8s.SecretKeySelector{
											Name: jsii.String("mariadb"),
											Key:  jsii.String(mysqlPasswordKey),
										},
									},
								},
							},
							EnvFrom: &[]*k8s.EnvFromSource{
								&k8s.EnvFromSource{SecretRef: &k8s.SecretEnvSource{
									Name: jsii.String("nextcloud"),
								}},
							},
						},
					},
				},
			},
		},
	})

	k8s.NewKubeIngress(chart, jsii.String("nextcloud-ingress"), &k8s.KubeIngressProps{
		Metadata: &k8s.ObjectMeta{
			Name: jsii.String("nextcloud-ingress"),
			Annotations: &map[string]*string{
				"kubernetes.io/ingress.class":         jsii.String("nginx"),
				"cert-manager.io/cluster-issuer":      jsii.String("letsencrypt-prod"),
				"cert-manager.io/acme-challenge-type": jsii.String("http01"),
			},
		},
		Spec: &k8s.IngressSpec{
			Tls: &[]*k8s.IngressTls{
				&k8s.IngressTls{
					Hosts:      jsii.Strings(nextcloudHost),
					SecretName: jsii.String("nextcloud-tls"),
				},
			},
			Rules: &[]*k8s.IngressRule{
				&k8s.IngressRule{
					Host: jsii.String(nextcloudHost),
					Http: &k8s.HttpIngressRuleValue{
						Paths: &[]*k8s.HttpIngressPath{
							&k8s.HttpIngressPath{
								Path: jsii.String("/"),
								PathType: jsii.String("Prefix"),
								Backend: &k8s.IngressBackend{
									Service: &k8s.IngressServiceBackend{
										Name: jsii.String("nextcloud"),
										Port: &k8s.ServiceBackendPort{
											Number: jsii.Number(80),
										},
									},
								},
							},
						},
					},
				},
			},
		},
	})

	return chart
}

func main() {
	app := cdk8s.NewApp(nil)
	NewMyChart(app, "hamravesh-task", nil)
	app.Synth()
}
